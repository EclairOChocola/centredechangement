<?php
  include_once(__DIR__. '/header.php');
  include_once(__DIR__. '/nav.php');
 ?>
<body>

  <div class="row">

    <div class="col l2 m2 hide-on-small-only red">

      <div class="row">
        <h5>Jean paul & Michel</h5>
      </div>

      <div class="row">
        <p>
          2 rue Paul Accolas
        </p>
        <p>
          36000 - Châteauroux
        </p>
      </div>

      <div class="row">
        <p>
          Rendez-vous et renseignement
        </p>
        <p>
          02 54 29 38 60
        </p>

      </div>

      <div class="row">
        <p>
          Mail: dumas.psy@gmail.com
        </p>
        <p>
          Siret: 481 717 270
        </p>
      </div>

    </div>

    <div class=" col l10 m10 s12">
      <div class="row">
        <h2 class="center-align">L' HYPNOSE ERICKSONIENNE ET LA PNL </h2>
      </div>

      <div class="row">

        <p>
          Pour vous débarrasser des comportements qui échappent à votre volonté
          ou encore...
        </p>

        <p>
          Mal-être, stress, dépression,angoisses, burn out, blocages,  traumatismes ,
          accident, décès, divorces, abus sexuel , excès de poids, Nutrition et
          nutrithérapie selon la methode CHATAIGNIER, anneau gastrique virtuel,
          deuil, boulimies, anorexie, phobie, manque de confiance, arrêt définitif du tabac,
          alcoolisme, violence conjugale, thérapie de couple, soutien traitement maladie lourde,
          insomnie, cauchemar, douleur, énurésie, minceur,  maladies de la peau,
          prise de décision importante, psoriasis, eczéma, urticaire,
          psychothérapie Ericksonienne, Formation thérapeute, Formation Hypnothérapeute,
          Formation hypnose Orléans, Formation hypnose Châteauroux, Hypnose Ericksonienne ...
        </p>

        <p>
          Pour tous les problèmes relevant de la médecine, le travail se fait sur
          les émotions et le renforcement des capacités de la personne et non sur
          les maladies qui sont du domaine médical.
        </p>

        <p>
          Pour Erickson,le créateur de l'Hypnose Ericksonienne, chacun a en lui
          les ressources, la capacité de soulager ses propres souffrances et de
          résoudre ses problèmes d'une manière qui ne doit pas nécessairement être
          comprise au niveau cognitif . Pour lui, il n'est pas important que qui que
          ce soit, même la personne elle-même, comprenne comment les changements se produisent.
          Il est seulement important qu'ils se produisent. L'inconscient accomplit des faits
          dont la conscience est incapable, et qu'elle ne conçoit souvent même pas.
          Mais c'est à condition que la conscience se borne à demander son secours à
          l'inconscient, sans préjuger ni de la manière, ni du moment qu'il choisira pour agir,
          et qu'elle s'abstienne entièrement d'interférer avec son action
        </p>

      </div>

    </div>







  </div>
</body>




<?php
  include_once(__DIR__. '/footer.php');
 ?>
