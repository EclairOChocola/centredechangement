<nav>
  <div class='nav-wrapper grey darken-1'>
    <a href='#!' class='brand-logo'>Logo</a>
    <a href='#' data-activates='mobile-nav' class='button-collapse'><i class='material-icons'>menu</i></a>
    <ul class='right hide-on-med-and-down'>
      <li><a href='./acceuil.html'> Accueil </a></li>
      <li><a href='#!'> Séance </a></li>
      <li><a href='#!'> Indication thérapeutique </a></li>
      <li><a href='#!'> Arret tabac </a></li>
      <li><a href='#!'> Historique </a></li>
      <li><a href='#!'> Votre thrapeute </a></li>
      <li><a href='#!'> Formation thérapeute </a></li>
      <li><a href='#!'> Annuaire </a></li>
    </ul>
    <ul class='side-nav' id='mobile-nav'>
      <li><a href='sass.html'>Sass</a></li>
      <li><a href='badges.html'>Components</a></li>
      <li><a href='collapsible.html'>Javascript</a></li>
      <li><a href='mobile.html'>Mobile</a></li>
    </ul>
  </div>
</nav>
